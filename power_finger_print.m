% Plot the measured current
function [duration, finger_print] = power_finger_print(log_idx, plot_on)
%power_finger_print(3,1);
if (nargin <2)
    plot_on = 1
end

data = importdata(sprintf('log3%04d.csv', log_idx));
ts = data(:, 1);
t = (ts-ts(1))/1e9;
inst_power = (data(:,2)/1e6) .* (data(:,3)/1e6);
duration = max(t)/60;
%figure
%plot(t, data(:, 2) / 1e6, 'r't) % API current_now
%hold on
%plot(t, data(:, 4) / 1e6, 'b') % Logical file current_now
%grid on



[f_den,x_den] = ksdensity(inst_power);
[pks,locs]=findpeaks(f_den);

for i = 1:length(locs)
    finger_print(i) = x_den(locs(i));
end




plot(x_den,f_den)
       

if (plot_on==1)
%figure
subplot(3, 1, 1)
title('Power')
plot(t, smooth(inst_power, 15), 'b');
xlabel('Time')
ylabel('Power')
subplot(3, 1, 2)
ksdensity(inst_power)
xlabel('power')
ylabel('PDF')
subplot(3, 1, 3)
cdfplot(inst_power)
xlabel('power')
ylabel('Empirical CDF')
subplot(3, 1, 3)
title('')
end

end