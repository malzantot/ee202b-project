package nesl.ucla.edu.androidpowerprofile;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by malzantot on 5/25/15.
 */
public class BrightnessAction implements Runnable {
    private Context mContext;
    private ContentResolver mContentResolver;
    private boolean isRunning = false;
    private final String LOG_TAG;
    private Handler mHandler;
    private int action = 0;

    public BrightnessAction(Context context, Handler handler) {
        this.mContext = context;
        this.mContentResolver = mContext.getContentResolver();
        this.LOG_TAG = mContext.getResources().getString(R.string.log_tag);
        this.mHandler = handler;
        try {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        try {
            Runtime.getRuntime().exec("su");
            Process set_user = Runtime.getRuntime().
                    exec("sudo echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
            Process mpd = Runtime.getRuntime().
                    exec("sudo stop mpdecision");
            Process cpu1 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu1/online");
            Process cpu2 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu2/online");
            Process cpu3 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu3/online");
            Process min_freq = Runtime.getRuntime().
                    exec("sudo echo 300000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
            Process max_freq = Runtime.getRuntime().
                    exec("sudo echo 2265600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");

            Process set_freq = Runtime.getRuntime().
                    exec("sudo echo 300000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        Log.d(LOG_TAG, "Running..");
        try {

            if (action == 0) {
                Log.d(LOG_TAG, "Action 0");

                android.provider.Settings.System.putInt(
                        mContext.getContentResolver(),
                        android.provider.Settings.System.SCREEN_BRIGHTNESS,
                        0);
                Thread.sleep(20000);

                android.provider.Settings.System.putInt(
                        mContext.getContentResolver(),
                        android.provider.Settings.System.SCREEN_BRIGHTNESS,
                        255);
                Thread.sleep(20000);

                mHandler.sendEmptyMessage(BrightnessPrintActivity.MSG_ACTION_STOPPED);


            } else if (action == 1) {
                Log.d(LOG_TAG, "Action 1");
                int currentLevel = 0;
                android.provider.Settings.System.putInt(
                        mContext.getContentResolver(),
                        android.provider.Settings.System.SCREEN_BRIGHTNESS,
                        currentLevel);
                Thread.sleep(8000);
                while (currentLevel <= 255) {
                    android.provider.Settings.System.putInt(
                            mContext.getContentResolver(),
                            android.provider.Settings.System.SCREEN_BRIGHTNESS,
                            currentLevel);
                    currentLevel += 16;
                    Thread.sleep(2000);
                    if (currentLevel == 256) currentLevel--;
                }
                Thread.sleep(8000);
                mHandler.sendEmptyMessage(BrightnessPrintActivity.MSG_ACTION_STOPPED);


            } else if (action == 2) {
                int cnt = 0;
                while (cnt < 100) {
                    int currentLevel = (cnt%2==0) ? 0: 255;
                    android.provider.Settings.System.putInt(
                            mContext.getContentResolver(),
                            android.provider.Settings.System.SCREEN_BRIGHTNESS,
                            currentLevel);
                    cnt++;
                    Thread.sleep(100);
                }
                mHandler.sendEmptyMessage(BrightnessPrintActivity.MSG_ACTION_STOPPED);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void start(int action) {
        this.action = action;
        isRunning = true;
        new Thread(this).start();

    }

    public void stop() {
        action = 0;isRunning = false;
    }
}
