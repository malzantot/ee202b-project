package nesl.ucla.edu.androidpowerprofile;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class BackgroundPowerMonitorActivity extends ActionBarActivity {
    private PowerMonitorService mPowerMonitorService;
    private boolean mBound = false;

    private Button mStartButton;
    private TextView mLogFileTextView;
    private final String LOG_TAG = "BackgroundActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background_power_monitor);

        mStartButton = (Button) findViewById(R.id.start_button);
        mLogFileTextView = (TextView) findViewById(R.id.power_log_tv);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mBound) {
            Log.d(LOG_TAG, "binding....");
            Notification notification = new Notification(R.drawable.icon,
                    getText(R.string.background_recording), System.currentTimeMillis());

            Intent intent = new Intent(this, PowerMonitorService.class);

            startService(intent);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mBound) {
            Log.d(LOG_TAG, "unbinding....");
            unbindService(mConnection);
            mBound = false;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_background_power_monitor, menu);
        return true;
    }


    public void onStartClick(View v) {
        if (mPowerMonitorService.isActive()) {
            mPowerMonitorService.stopMonitoring();
            mStartButton.setText(R.string.start_button);
            mLogFileTextView.setText(mPowerMonitorService.getLogFileName());

        } else {
            mPowerMonitorService.startMonitoring();
            mStartButton.setText(R.string.stop_button);
            mLogFileTextView.setText(mPowerMonitorService.getLogFileName());
        }
    }

    public void onLogClick(View  v) {
        Log.d("TAG", mPowerMonitorService.getStartTime());
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(LOG_TAG, "Service connected..");
            mBound = true;
            PowerMonitorService.PowerMonitorBinder pmBinder =
                    (PowerMonitorService.PowerMonitorBinder) service;
            mPowerMonitorService = pmBinder.getService();
            mLogFileTextView.setText(mPowerMonitorService.getLogFileName());
            if (mPowerMonitorService.isActive()) {
                mStartButton.setText(R.string.stop_button);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(LOG_TAG, "Service disconnected..");
            mBound = false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
