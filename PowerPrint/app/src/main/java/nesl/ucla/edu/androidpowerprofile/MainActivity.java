package nesl.ucla.edu.androidpowerprofile;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onBrightnessClick(View v) {
        startActivity(new Intent(this, BrightnessPrintActivity.class));
    }

    public void onBackgroundRecord(View v){
        startActivity(new Intent(this, BackgroundPowerMonitorActivity.class));
    }

    public void onBackCPUClick(View v){
        startActivity(new Intent(this, CPUBackgroundActivity.class));
    }

    public void onCPUClick(View v)
    {
        // startActivity(new Intent(this, CPUBackgroundActivity.class));
        startActivity(new Intent(this, CPUPrintActivity.class));
    }

    public void onWifiClick(View v)
    {
        // startActivity(new Intent(this, CPUBackgroundActivity.class));
        startActivity(new Intent(this, WifiActivity.class));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
