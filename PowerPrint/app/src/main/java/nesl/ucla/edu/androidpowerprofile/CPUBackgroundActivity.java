package nesl.ucla.edu.androidpowerprofile;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by Alanwar on 5/31/2015.
 */
public class CPUBackgroundActivity extends ActionBarActivity {
    private PowerMonitorService mPowerMonitorService;
    private CPUService mCPUService;

    private boolean mBound = false;
    private boolean mBound_CPU = false;
    private Button mStartButton;
    private TextView action_log_tv;
    private TextView power_log_tv;
    private final String LOG_TAG = "BackgroundActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpubackground);

        try {
            mStartButton = (Button) findViewById(R.id.start_button);
        }
        catch (Exception ex)
        {
            Log.d(LOG_TAG, "What !!");
        }
        action_log_tv = (TextView) findViewById(R.id.action_log_tv);
        power_log_tv = (TextView) findViewById(R.id.pow_log_tv);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mBound) {
            Log.d(LOG_TAG, "binding....");
            Notification notification = new Notification(R.drawable.icon,
                    getText(R.string.background_recording), System.currentTimeMillis());

            //start pow service
            Intent pow_intent = new Intent(this, PowerMonitorService.class);
            startService(pow_intent);
            bindService(pow_intent, pow_mConnection, Context.BIND_AUTO_CREATE);

            //start cpu service
            Intent intent = new Intent(this, CPUService.class);
            startService(intent);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);


        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mBound) {
            Log.d(LOG_TAG, "unbinding....");
            unbindService(mConnection);
            unbindService(pow_mConnection);
            mBound = false;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_background_power_monitor, menu);
        getMenuInflater().inflate(R.menu.menu_cpubackground, menu);
        return true;
    }


    public void onStartClick(View v)
    {
        if (mPowerMonitorService.isActive())
        {
            mPowerMonitorService.stopMonitoring();
            mStartButton.setText(R.string.start_button);

            power_log_tv.setText("NA");
        } else
        {
            mPowerMonitorService.startMonitoring();
            mStartButton.setText(R.string.stop_button);
            power_log_tv.setText(mPowerMonitorService.getLogFileName());

        }

        if (mCPUService.isActive()) {
            mCPUService.stopMonitoring();
            action_log_tv.setText("NA");

        } else {
            mCPUService.startMonitoring();
            action_log_tv.setText(mCPUService.getLogFileName());
        }



    }

    public void onLogClick(View  v) {
        Log.d("TAG", mCPUService.getStartTime());
    }

    protected ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            Log.d(LOG_TAG, "Service connected..");
            mBound_CPU = true;
            CPUService.CPUBinder CPU_pmBinder =
                    (CPUService.CPUBinder) service;
            mCPUService = CPU_pmBinder.getService();
         //   mLogFileTextView.setText(mCPUService.getLogFileName());
            if (mCPUService.isActive()) {
                mStartButton.setText(R.string.stop_button);
            }
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(LOG_TAG, "Service disconnected..");
            mBound_CPU = false;
        }
    };

    private ServiceConnection pow_mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(LOG_TAG, "Service connected..");
            mBound = true;
            PowerMonitorService.PowerMonitorBinder pmBinder =
                    (PowerMonitorService.PowerMonitorBinder) service;
            mPowerMonitorService = pmBinder.getService();
        //    mLogFileTextView.setText(mPowerMonitorService.getLogFileName());
            if (mPowerMonitorService.isActive()) {
                mStartButton.setText(R.string.stop_button);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(LOG_TAG, "Service disconnected..");
            mBound = false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
