package nesl.ucla.edu.androidpowerprofile;

/**
 * Created by Alanwar on 5/29/2015.
 */

import android.content.ContentResolver;
import android.content.Context;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class CPUPrintAction implements Runnable {
    private Context mContext;
    private ContentResolver mContentResolver;
    private boolean isRunning = false;
    private final String LOG_TAG;
    private final static int INTERVAL=  30000;
    int stateIdx = 0;
    int index;
    FileOutputStream outputStream_act;
    PrintWriter pw_act;
    File ff_act;

    private String mLogFileName;


    public CPUPrintAction(Context context) {
        this.mContext = context;
        this.mContentResolver = mContext.getContentResolver();

        try {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

        } catch(Exception ex) {
            ex.printStackTrace();
        }
        android.provider.Settings.System.putInt(
                mContext.getContentResolver(),
                android.provider.Settings.System.SCREEN_BRIGHTNESS,
                10);


        this.LOG_TAG = mContext.getResources().getString(R.string.log_tag);
        stateIdx = 0;

        try {
            Runtime.getRuntime().exec("su");
            Process set_freq = Runtime.getRuntime().
                    exec("sudo echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
            Process mpd = Runtime.getRuntime().
                    exec("sudo stop mpdecision");
            Process cpu1 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu1/online");
            Process cpu2 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu2/online");
            Process cpu3 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu3/online");
            Process min_freq = Runtime.getRuntime().
                    exec("sudo echo 300000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
            Process max_freq = Runtime.getRuntime().
                    exec("sudo echo 2265600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");

        } catch (IOException e) {
            e.printStackTrace();
        }


/*
        try {
            File logDir = new File(Environment.getExternalStorageDirectory(), "PowerPrint");
            File ff_act = new File(logDir, "CPU_act.txt");
            Log.d(LOG_TAG, ff_act.getAbsolutePath());

        }catch (Exception ex) {
            outputStream_act = null;
            Log.d(LOG_TAG, ex.getMessage());
            ex.printStackTrace();;
        }
        */
    }

    private int [] freq_arr = { 0,4,8,12};

    public void run() {
        Log.d(LOG_TAG, "Running..");


        try {

            //pw_act.write("start \n");

            while (isRunning) {
                publishProgress(index);
               // stateIdx = (stateIdx + 3) % 14;
                stateIdx=  (stateIdx +1) % 4;
                index = freq_arr[stateIdx];
                Thread.sleep(INTERVAL);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void publishProgress(Integer... values){


        switch ((int)values[0])
        {
            // s4 384000 ,486000, 594000 , 702000 ,810000 ,918000 ,1026000, 1134000 , 1242000, 1350000, 1458000,1566000 ,  1674000 , 1728000

            //nexus  300000 422400 652800 729600 883200 960000 1036800 1190400 1267200 1497600 1574400 1728000 1958400 2265600
            case 0:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));
                    String temp =inGet_freq.readLine();
                    Log.d(LOG_TAG, temp);
                    pw_act.write(System.nanoTime() + ", " + temp + "\n");
                    inGet_freq.close();

                    //  Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                    exec("sudo echo 300000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                   //exec("sudo echo 384000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION 0 !! " + ex.getMessage());
                    ex.printStackTrace(); }

                break;
            }
            //  background_layout.setBackgroundColor(Color.BLACK);

            case 1:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));
                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();

               //     Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                          //  exec("sudo echo 486000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                      exec("sudo echo 422400 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 2:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();


                 //   Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                    //        exec("sudo echo 594000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                            exec("sudo echo 652800 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 3:
            {
                try
                {

                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));
                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();


                   // Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                     //       exec("sudo echo 702000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                             exec("sudo echo 729600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 4:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() + "\n");
                    inGet_freq.close();


             //       Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                            //exec("sudo echo 810000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                                    exec("sudo echo 883200 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION 0 !! " + ex.getMessage());
                    ex.printStackTrace(); }

                break;
            }
            case 5:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));
                    inGet_freq.close();

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");

               //     Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                      //      exec("sudo echo 918000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                    exec("sudo echo 960000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 6:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");

                    inGet_freq.close();

                 //   Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                      //      exec("sudo echo 1026000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                              exec("sudo echo 1036800 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 7:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");

                    inGet_freq.close();

                //    Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                       //     exec("sudo echo 1134000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                               exec("sudo echo 1190400 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 8:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();

                //    Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                        //    exec("sudo echo 1242000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                                exec("sudo echo 1267200 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION 0 !! " + ex.getMessage());
                    ex.printStackTrace(); }

                break;
            }
            case 9:
            {
                try
                {

                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() + "\n");
                    inGet_freq.close();


                //    Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                          //  exec("sudo echo 1350000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                                  exec("sudo echo 1497600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 10:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();

                //    Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                        //    exec("sudo echo 1458000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                                exec("sudo echo 1574400 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 11:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();

                //    Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                           // exec("sudo echo 1566000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                               exec("sudo echo 1728000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !!");
                    ex.printStackTrace(); }

                break;
            }
            case 12:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();

                 //   Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                         //   exec("sudo echo 1674000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                                 exec("sudo echo 1958400 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION 0 !! " + ex.getMessage());
                    ex.printStackTrace(); }

                break;
            }
            case 13:
            {
                try
                {
                    Process pGet_freq =  Runtime.getRuntime().exec("cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
                    BufferedReader inGet_freq = new BufferedReader(
                            new InputStreamReader(pGet_freq.getInputStream()));

                    pw_act.write(System.nanoTime() + ", " + inGet_freq.readLine() +  "\n");
                    inGet_freq.close();

                 //   Runtime.getRuntime().exec("su");
                    Process set_freq = Runtime.getRuntime().
                        //    exec("sudo echo 1728000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
                                exec("sudo echo 2265600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");


                } catch (Exception ex) {
                    Log.d(LOG_TAG, "EXCEPTION !! ");
                    ex.printStackTrace(); }

                break;
            }
            default:

        }
    }

    public String getLogFileName() {
        return this.mLogFileName;
    }

    private File getLogFile() {
        try {
            File logDir = new File(Environment.getExternalStorageDirectory(), "CPUPrint");
            if (!logDir.exists()) {
                Log.d(LOG_TAG, "Dir not existing, will create : " + logDir.getAbsolutePath());
                logDir.mkdir();
            }
            int logIndex = 1;
            File [] existingLogs = logDir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().startsWith("cpu_action");
                }
            });
            if (existingLogs != null)
                logIndex = existingLogs.length + 1;



            String phoneId = ((PowerPrintApplication) mContext.getApplicationContext()).getPhoneID();
            mLogFileName = String.format("cpu_action%s%04d.csv",phoneId,logIndex);
            File newLogFile = new File(logDir, mLogFileName);

            return newLogFile;

        } catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void start() {
     try{
        isRunning = true;
        ff_act = getLogFile();
        if (ff_act == null) return;
        outputStream_act = new FileOutputStream(ff_act);
        pw_act = new PrintWriter(outputStream_act);
    } catch (Exception ex)
     {
        ex.printStackTrace();
    }

        new Thread(this).start();

    }

    public void stop() {
        isRunning = false;
        pw_act.flush();
        pw_act.close();
    }
}
