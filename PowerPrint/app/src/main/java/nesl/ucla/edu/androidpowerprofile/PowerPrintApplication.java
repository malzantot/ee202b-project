package nesl.ucla.edu.androidpowerprofile;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Created by malzantot on 6/3/15.
 */
public class PowerPrintApplication extends Application {
    String phoneID = "0";

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            File phoneIDFile = new File(Environment.getExternalStorageDirectory(), "phone_id");
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(phoneIDFile)));
            phoneID = reader.readLine();
            reader.close();
            Log.d("App class", phoneID);
        }catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public String getPhoneID() {
        return this.phoneID;
    }
}
