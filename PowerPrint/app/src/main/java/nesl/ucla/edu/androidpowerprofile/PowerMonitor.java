package nesl.ucla.edu.androidpowerprofile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by malzantot on 5/25/15.
 */
public class PowerMonitor implements Runnable {

    private String mType;
    private final String currentNowCmd;
    private final String voltageNowCmd;
    private boolean mRunning;
    private Context mContext;
    private PowerManager mPM;
    private PowerManager.WakeLock wl;
    private BatteryManager batteryManager;

    private FileOutputStream fosOutputFile;
    private PrintWriter pwOutputFile;
    private String mLogFileName;

    private final String LOG_TAG;
    private List<Long> current_log;
    private List<Long> time_stamp;
    private List<Integer> voltage_log;
    private int current_voltage;
    private IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    String AcType;
    public PowerMonitor(Context context, String type) {
        this.mContext = context;
        this.mType = type;
        this.mRunning = false;
        this.fosOutputFile = null;
        this.pwOutputFile = null;
        this.LOG_TAG = mContext.getResources().getString(R.string.log_tag);
        this.currentNowCmd = mContext.getResources().getString(R.string.read_current_cmd);
        this.voltageNowCmd = mContext.getResources().getString(R.string.read_voltage_cmd);
        this.mPM = (PowerManager) mContext.getSystemService(mContext.POWER_SERVICE);
        this.batteryManager = (BatteryManager) mContext.getSystemService(mContext.BATTERY_SERVICE);
        this.current_log = new ArrayList<Long>();
        this.time_stamp = new ArrayList<Long>();
        this.voltage_log =  new ArrayList<Integer>();
this.AcType = type;

    }

    public void run() {

        while (mRunning) {
        try {

                Process psCurrentNow = Runtime.getRuntime().exec(this.currentNowCmd);
                Process psVoltageNow = Runtime.getRuntime().exec(this.voltageNowCmd);
                InputStreamReader isrCurrent =  new InputStreamReader(psCurrentNow.getInputStream());
                InputStreamReader isrVoltage = new InputStreamReader(psVoltageNow.getInputStream());
                BufferedReader brCurrentNow = new BufferedReader(isrCurrent);
                BufferedReader brVoltageNow = new BufferedReader(isrVoltage);
                pwOutputFile.println(System.nanoTime() + ", " +
                                brCurrentNow.readLine() + ", " +
                                brVoltageNow.readLine()
                );

                isrCurrent.close();
                isrVoltage.close();

           /*
            long current = batteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
                //pwOutputFile.println(System.nanoTime() + ", " + current);
                time_stamp.add(System.nanoTime());
                current_log.add(current);
                voltage_log.add(current_voltage);
                */
                //Thread.sleep(10);
            } catch(Exception ex) {
                ex.printStackTrace();
            }


        }
        if(pwOutputFile != null) {
            int log_cnt = time_stamp.size();
            for (int ii = 0; ii <log_cnt; ii++) {
                pwOutputFile.println(time_stamp.get(ii) + ", " + current_log.get(ii) + ", " + voltage_log.get(ii) );
            }
            pwOutputFile.close();
        }
    }

    /*
    BroadcastReceiver batteryLevelReciever = new BroadcastReceiver() {


        @Override
        public void onReceive(Context context, Intent intent) {
            current_voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
        }
    };
    */
    public boolean start() {
        File logFile = getLogFile();
        if (logFile != null) {
            try {
                Log.d(LOG_TAG, "Location = " + logFile.getAbsolutePath());
                pwOutputFile = new PrintWriter(logFile);
                time_stamp.clear();
                current_log.clear();
                mRunning = true;
                this.wl = mPM.newWakeLock(PowerManager.FULL_WAKE_LOCK, LOG_TAG);
                this.wl.acquire();
               // mContext.registerReceiver(batteryLevelReciever, batteryLevelFilter);
                new Thread(this).start();
                return true;
            }catch(Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
        Log.d(LOG_TAG, "Error happened, something bad... :(");
        return false;
    }

    public boolean isActive() {
        return mRunning;
    }

    public void stop() {
       // mContext.unregisterReceiver(batteryLevelReciever);
        mRunning = false;
        this.wl.release();

    }

    public String getLogFileName() {
        return this.mLogFileName;
    }

    private File getLogFile() {
        try {

            File logDir = new File(Environment.getExternalStorageDirectory(), AcType);
            //File logDir = new File("/storage/sdcard0/", "PowerPrint.txt");
            if (!logDir.exists()) {
                Log.d(LOG_TAG, "Dir not existing, will create : " + logDir.getAbsolutePath());
                logDir.mkdir();
            }
            int logIndex = 1;
            File [] existingLogs = logDir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().startsWith("lcdlog");
                }
            });
            if (existingLogs != null)
                logIndex = existingLogs.length + 1;

            //logDir.listFiles() == null ? 0 : logDir.listFiles().length;
            String phoneId = ((PowerPrintApplication) mContext.getApplicationContext()).getPhoneID();
            mLogFileName = String.format("%slog%s%04d.csv", this.AcType ,phoneId,logIndex);
            File newLogFile = new File(logDir, mLogFileName);
            return newLogFile;

        } catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
