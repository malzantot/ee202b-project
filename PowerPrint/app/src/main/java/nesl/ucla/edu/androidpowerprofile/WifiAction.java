package nesl.ucla.edu.androidpowerprofile;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import java.util.List;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
/**
 * Created by alanwar on 6/14/15.
 */
public class WifiAction implements Runnable{
    private Context mContext;
    private ContentResolver mContentResolver;
    private boolean isRunning = false;
    private final String LOG_TAG;
    private final static int INTERVAL=  30000;
    int stateIdx = 0;
    int index;
    FileOutputStream outputStream_act;
    PrintWriter pw_act;
    File ff_act;
    private String mLogFileName;
    WifiManager mainWifi;

    public WifiAction(Context context)
    {
        this.mContext = context;
        this.mContentResolver = mContext.getContentResolver();

        try {
            Settings.System.putInt(mContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

        } catch(Exception ex) {
            ex.printStackTrace();
        }
        android.provider.Settings.System.putInt(
                mContext.getContentResolver(),
                android.provider.Settings.System.SCREEN_BRIGHTNESS,
                10);


        this.LOG_TAG = mContext.getResources().getString(R.string.log_tag);
        stateIdx = 0;

        try {
            Runtime.getRuntime().exec("su");
            Process usrspace = Runtime.getRuntime().
                    exec("sudo echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
            Process mpd = Runtime.getRuntime().
                    exec("sudo stop mpdecision");
            Process cpu1 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu1/online");
            Process cpu2 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu2/online");
            Process cpu3 = Runtime.getRuntime().
                    exec("sudo echo 0 > /sys/devices/system/cpu/cpu3/online");
            Process min_freq = Runtime.getRuntime().
                    exec("sudo echo 300000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
            Process max_freq = Runtime.getRuntime().
                    exec("sudo echo 2265600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
            Process set_freq = Runtime.getRuntime().
                            exec("sudo echo 2265600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");

            // Initiate wifi service manager
            mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);


         /*   if( mainWifi.isWifiEnabled() == false )
            {
                mainWifi.setWifiEnabled(true);
            }*/

        } catch (IOException e) {
            Log.d(LOG_TAG, "EXCEPTION !! ");
            e.printStackTrace();
        }

    }


    public String getLogFileName() {
        return this.mLogFileName;
    }

    private File getLogFile() {
        try {
            File logDir = new File(Environment.getExternalStorageDirectory(), "Wifiprint");
            if (!logDir.exists()) {
                Log.d(LOG_TAG, "Dir not existing, will create : " + logDir.getAbsolutePath());
                logDir.mkdir();
            }
            int logIndex = 1;
            File [] existingLogs = logDir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().startsWith("wifi");
                }
            });
            if (existingLogs != null)
                logIndex = existingLogs.length + 1;



            String phoneId = ((PowerPrintApplication) mContext.getApplicationContext()).getPhoneID();
            mLogFileName = String.format("wifi%s%04d.csv",phoneId,logIndex);
            File newLogFile = new File(logDir, mLogFileName);

            return newLogFile;

        } catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void run() {
        Log.d(LOG_TAG, "Running..");

        try {

            //pw_act.write("start \n");

            while (isRunning) {
              try {
                  mainWifi.startScan();
              }
              catch (Exception ex) {
                  Log.d(LOG_TAG, "Wifi EXCEPTION !! ");
                    ex.printStackTrace();
                }
                pw_act.write(System.nanoTime()  + "\n");

                Thread.sleep(INTERVAL);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void start() {
        try{
            isRunning = true;
            ff_act = getLogFile();
            if (ff_act == null) return;
            outputStream_act = new FileOutputStream(ff_act);
            pw_act = new PrintWriter(outputStream_act);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }

        new Thread(this).start();

    }

    public void stop() {
        isRunning = false;
        pw_act.flush();
        pw_act.close();
    }

}
