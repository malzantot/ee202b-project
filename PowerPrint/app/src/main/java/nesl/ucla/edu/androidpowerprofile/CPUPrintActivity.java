package nesl.ucla.edu.androidpowerprofile;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class CPUPrintActivity extends ActionBarActivity {

    private PowerMonitor mPowerMonitor;
    private CPUPrintAction mCPUPrintAction;
    private TextView action_log_tv;
    private TextView power_log_tv;
    private Button btnStart;

    private static final String LOG_TAG = "CPUPrint";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpuprint);
        btnStart = (Button) findViewById(R.id.start_button);

        action_log_tv = (TextView) findViewById(R.id.action_log_tv);
        power_log_tv = (TextView) findViewById(R.id.pow_log_tv);

        mCPUPrintAction = new CPUPrintAction(this);
        mPowerMonitor = new PowerMonitor(this, "CPUPrint");


    }

    public void onStartClick(View v)
    {
        if (!mPowerMonitor.isActive())
        {
            mPowerMonitor.start();
            mCPUPrintAction.start();
            power_log_tv.setText(mPowerMonitor.getLogFileName());
            action_log_tv.setText(mCPUPrintAction.getLogFileName());
            btnStart.setText(getString(R.string.stop_button));
        } else
        {
            mPowerMonitor.stop();
            mCPUPrintAction.stop();
            power_log_tv.setText("NA");
            btnStart.setText(getString(R.string.start_button));
            action_log_tv.setText("NA");


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_brightness_print, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpuprint);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cpuprint, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
