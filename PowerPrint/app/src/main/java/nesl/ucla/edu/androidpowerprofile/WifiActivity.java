package nesl.ucla.edu.androidpowerprofile;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WifiActivity extends ActionBarActivity {
    private PowerMonitor mPowerMonitor;
    private WifiAction mWifiAction;
    private TextView wifi_log_tv;
    private TextView power_log_tv;
    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);
        btnStart = (Button) findViewById(R.id.start_button);

        wifi_log_tv = (TextView) findViewById(R.id.action_log_tv);
        power_log_tv = (TextView) findViewById(R.id.pow_log_tv);

        mWifiAction = new WifiAction(this);
        mPowerMonitor = new PowerMonitor(this, "wifiprint");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wifi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onStartClick(View v)
    {
        if (!mPowerMonitor.isActive())
        {
            mPowerMonitor.start();
            mWifiAction.start();
            power_log_tv.setText(mPowerMonitor.getLogFileName());
            wifi_log_tv.setText(mWifiAction.getLogFileName());
            btnStart.setText(getString(R.string.stop_button));
        } else
        {
            mPowerMonitor.stop();
            mWifiAction.stop();
            power_log_tv.setText("NA");
            btnStart.setText(getString(R.string.start_button));
            wifi_log_tv.setText("NA");


        }
    }
}
