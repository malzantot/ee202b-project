package nesl.ucla.edu.androidpowerprofile;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

/**
 * Created by malzantot on 5/26/15.
 */
public class PowerMonitorService extends Service {
    private IBinder mBinder;

    private PowerMonitor mPowerMonitor;
    private boolean mIsActive = false;
    private String mStartTime;
    private NotificationManager mNM;
    private int notificatoinID = 1115;

    @Override
    public void onCreate() {
        this.mStartTime = new Date().toString();
        //Log.d("Service", "Started at : " + this.mStartTime);
        mBinder = new PowerMonitorBinder();
        mPowerMonitor = new PowerMonitor(getApplicationContext(), "Background");
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "onStartCommand for service created at : " + mStartTime);
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        mNM.cancel(notificatoinID);

        // Tell the user we stopped.
        Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
        Log.d("Service", "Destroying.." );
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class PowerMonitorBinder extends Binder {
        PowerMonitorService getService() {
            return PowerMonitorService.this;
        }
    }

    public boolean isActive() {
        return mIsActive;
    }

    public void startMonitoring() {
        if (!mIsActive) {
            mIsActive = true;
            mPowerMonitor.start();
            Notification note = new Notification(R.drawable.icon,
                    getText(R.string.background_recording),
                    System.currentTimeMillis());
            Intent i = new Intent(this, BackgroundPowerMonitorActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pi = PendingIntent.getActivity(this,0, i, 0);
            note.setLatestEventInfo(this, getText(R.string.app_name), getLogFileName(), pi);

            note.flags |= Notification.FLAG_NO_CLEAR;

            startForeground(notificatoinID, note);
            mNM.notify(notificatoinID, note);

        }
    }

    public void stopMonitoring() {
        if (mIsActive) {
            mNM.cancel(notificatoinID);
            mPowerMonitor.stop();
            mIsActive = false;
            stopForeground(true);
        }
    }

    public String getLogFileName() {
        if (mIsActive) {
            return mPowerMonitor.getLogFileName();
        }
        return "NA";
    }

    public String getStartTime() {
        return this.mStartTime;
    }
}
