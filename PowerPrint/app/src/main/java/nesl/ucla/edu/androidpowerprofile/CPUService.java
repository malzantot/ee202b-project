package nesl.ucla.edu.androidpowerprofile;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

public class CPUService extends Service {
    private IBinder mBinder_CPU;

    private CPUPrintAction mCPUMonitor;
    private boolean mIsActive = false;
    private String mStartTime;
    private NotificationManager mNM;
    private int notificatoinID = 1117;

    @Override
    public void onCreate() {
        this.mStartTime = new Date().toString();
        Log.d("Service", "Started at : " + this.mStartTime);
        mBinder_CPU = new CPUBinder();
        mCPUMonitor = new CPUPrintAction(getApplicationContext());
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "onStartCommand for service created at : " + mStartTime);
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        mNM.cancel(notificatoinID);

        // Tell the user we stopped.
        Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
        Log.d("Service", "Destroying..");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder_CPU;
    }

    public class CPUBinder extends Binder {
        CPUService getService() {
            return CPUService.this;
        }
    }

    public boolean isActive() {
        return mIsActive;
    }

    public void startMonitoring() {
        if (!mIsActive) {
            mIsActive = true;
            mCPUMonitor.start();
            Notification note = new Notification(R.drawable.icon,
                    getText(R.string.background_recording),
                    System.currentTimeMillis());
            Intent i = new Intent(this, CPUBackgroundActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);
            note.setLatestEventInfo(this, getText(R.string.app_name), getLogFileName(), pi);

            note.flags |= Notification.FLAG_NO_CLEAR;

            startForeground(notificatoinID, note);
            mNM.notify(notificatoinID, note);

        }

    }

    public void stopMonitoring() {
        if (mIsActive) {
            mNM.cancel(notificatoinID);
            mCPUMonitor.stop();
            mIsActive = false;
            stopForeground(true);
        }
    }

    public String getLogFileName() {
        if (mIsActive) {
            return mCPUMonitor.getLogFileName();
        }
        return "NA";
    }

    public String getStartTime() {
        return this.mStartTime;
    }
}
    /*public CPUService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}*/
