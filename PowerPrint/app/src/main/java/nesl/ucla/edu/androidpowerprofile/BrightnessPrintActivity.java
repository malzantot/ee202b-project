package nesl.ucla.edu.androidpowerprofile;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class BrightnessPrintActivity extends ActionBarActivity {

    private PowerMonitor mPowerMonitor;
    private BrightnessAction mBrightnessAction;
    private TextView log_file_tv;

    private Button start_button;
    private Button startAction2;
    private Button startAction3;

    public static final int MSG_ACTION_STOPPED = 1;

    final Handler actionHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what  == MSG_ACTION_STOPPED) {
                mPowerMonitor.stop();
                mBrightnessAction.stop();
                log_file_tv.setText(mPowerMonitor.getLogFileName());
                start_button.setText(getString(R.string.start_button));
                startAction2.setText(getString(R.string.start_action2));
                startAction3.setText(getString(R.string.start_action3));


            }
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brightness_print);
        log_file_tv = (TextView) findViewById(R.id.power_log_tv);
        start_button = (Button) findViewById(R.id.start_button);
        startAction2 = (Button) findViewById(R.id.start_action2);
        startAction3 = (Button) findViewById(R.id.start_action3);

        mBrightnessAction = new BrightnessAction(this, actionHandler);
        mPowerMonitor = new PowerMonitor(this, "lcd");
    }

    public void onStartClick(View v) {
        if (!mPowerMonitor.isActive()) {
            mPowerMonitor.start();
            mBrightnessAction.start(0);
            log_file_tv.setText(mPowerMonitor.getLogFileName());
            start_button.setText(getString(R.string.stop_button));
        } else {
            mPowerMonitor.stop();
            mBrightnessAction.stop();
            log_file_tv.setText(mPowerMonitor.getLogFileName());
            start_button.setText(getString(R.string.start_button));


        }
    }


    public void onStartAction2Click(View v) {
        if (!mPowerMonitor.isActive()) {
            mPowerMonitor.start();
            mBrightnessAction.start(1);
            log_file_tv.setText(mPowerMonitor.getLogFileName());
            startAction2.setText(getString(R.string.stop_action2));
        } else {
            mPowerMonitor.stop();
            mBrightnessAction.stop();
            log_file_tv.setText(mPowerMonitor.getLogFileName());
            startAction2.setText(getString(R.string.start_action2));


        }
    }

    public void onStartAction3Click(View v) {
        if (!mPowerMonitor.isActive()) {
            mPowerMonitor.start();
            mBrightnessAction.start(2);
            log_file_tv.setText(mPowerMonitor.getLogFileName());
            startAction3.setText(getString(R.string.stop_action3));
        } else {
            mPowerMonitor.stop();
            mBrightnessAction.stop();
            log_file_tv.setText(mPowerMonitor.getLogFileName());
            startAction3.setText(getString(R.string.start_action3));


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_brightness_print, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
