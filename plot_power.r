rm(list = ls())
setwd('/Users/malzantot/Documents/MyUCLA/EE202B/Project')
library(ggplot2)

current_data <- read.csv('current_log.txt', header=FALSE)
voltage_data <- read.csv('voltage_log.txt', header=FALSE)
row_count = min(nrow(current_data), nrow(voltage_data))
plot(seq(row_count), current_data[1:row_count,])
plot(seq(row_count), voltage_data[1:row_count,])

power.df <- data.frame(index=seq(row_count),
                       current=current_data[1:row_count,],
                       voltage=voltage_data[1:row_count,])

#power.df <- power.df[1000:nrow(power.df),]                       
p <- ggplot(power.df, aes(x=index, y=(current/1e6)*voltage)) +
  geom_line(color='blue') # + geom_point(color='blue') 
print(p)

