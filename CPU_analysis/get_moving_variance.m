function m  = get_moving_variance(phone_id,log_idx)
%data = importdata(sprintf('/Users/alanwar/Desktop/logs/five_phone_v3/log%d%04d.csv', phone_id,log_idx));
data= importdata(sprintf('/Users/alanwar/Desktop/logs/wifi/wifi_v1/log%d%04d.csv', phone_id,log_idx));
data = data(1:3000,1:3);
win_size = 32;
ts = data(:, 1);
t = (ts-ts(1))/1e9;
input_x = (data(:,2)/1e6) .* (data(:,3)/1e6);
duration = max(t)/60;


%temp = length(data(:,1))/2;

if (nargin < 2)
    win_size = 16;
end
moving_var = [];
log_len = length(input_x);
for k = win_size:log_len - win_size
        start_idx = max(1, k-win_size);
        end_idx = min(log_len, k+win_size);
        moving_var(k) = var(input_x(start_idx:end_idx));
end

plot(moving_var)
m = max(moving_var)
end