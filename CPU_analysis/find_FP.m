function [ FP ] = find_FP( data )
%DIVIDE_FP Summary of this function goes here
%   Detailed explanation goes here

inst_power = (data(:,2)/1e6) .* (data(:,3)/1e6);


[f_den,x_den] = ksdensity(inst_power);

[pks,locs]=findpeaks(f_den);

for i = 1:length(locs)
    FP(i) = x_den(locs(i));
    prop(i) = f_den(locs(i));
end


end
