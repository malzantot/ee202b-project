% Plot the measured current
function [duration, finger_print] = power_finger_print(phone_id,log_idx, plot_on)
%power_finger_print(3,1);
if (nargin <2)
    plot_on = 1
end

%data = importdata(sprintf('/Users/alanwar/Desktop/logs/five_phone_v3/log%d%04d.csv', phone_id,log_idx));
data = importdata(sprintf('/Users/alanwar/Documents/ee202b-project/CPU_mat_logs/five_phone_v3/log%d%04d.csv', phone_id,log_idx));
%data = data(,1:3);
%temp = length(data(:,1))/2;
index_1 = floor(length(data)/3);
index_2 = floor(length(data)*2/3);
%data= data(1:index_1,1:3);

%data(1:index_1,1:3) 
%data(index_1+1:index_2,1:3) 
%data(index_2+1:index_3,1:3) 
%data(index_2+1:end,1:3

ts = data(:, 1);
t = (ts-ts(1))/1e9;
inst_power = (data(:,2)/1e6) .* (data(:,3)/1e6);
duration = max(t)/60;
%figure
%plot(t, data(:, 2) / 1e6, 'r't) % API current_now
%hold on
%plot(t, data(:, 4) / 1e6, 'b') % Logical file current_now
%grid on



%[f_den,x_den] = ksdensity(smooth(inst_power,70));
[f_den,x_den] = ksdensity(inst_power);

[pks,locs]=findpeaks(f_den);

for i = 1:length(locs)
    finger_print(i) = x_den(locs(i));
    prop(i) = f_den(locs(i));
end




plot(x_den,f_den)
       

if (plot_on==1)
%figure
subplot(3, 1, 1)
title('Power')
plot(t, smooth(inst_power, 15), 'b');
%hold on 
%for(x = finger_print)
%%plot(t,x)
%end

hold off
xlabel('Time')
ylabel('Power')

subplot(3, 1, 2)
ksdensity(inst_power)
xlabel('power')
ylabel('PDF')
%hold on 
%plot(finger_print,prop,'*')
%hold off


subplot(3, 1, 3)
cdfplot(inst_power)
%xlabel('power')
%ylabel('Empirical CDF')
subplot(3, 1, 3)
title('')
end
%%/Users/alanwar/Documents/wifi_logs/
%1- scan 
%2- no scan 
%3- finger_print =0.5077    0.8020    1.2924    1.6848
% 5..3  scan
% 5..4 no scan (wifi on )
% 5..5 wifi off
% 5..6 no scan wifi on again 
end