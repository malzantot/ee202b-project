clear all
% %% v1; v2; v3
% ph2 =[0.4804 0.7455 1.1873 1.5407; 0.4859 0.7305 1.1925 1.6001;0.4790 0.7219 1.1636 1.5832];
% ph3 =[0.5396 0.8394 1.3392 1.7057; 0.5299 0.7998 1.3397 1.7252;0.5227 0.8236 1.3253	1.7266];
% ph4 =[0.5027 0.7320 1.1713 1.4960; 0.4929 0.7196 1.1319 1.4617;0.4954 0.7155 1.1356 1.4757];
% ph5 =[0.5443 0.8192 1.3189 1.7937; 0.5227 0.7983 1.2995 1.7756;0.5318 0.7968 1.2784 1.7360];
% ph6 =[0.5079 0.7964 1.2404 1.6177; 0.4765 0.7406 1.2028 1.6650;0.4879 0.7471 1.2006 1.5677];
% ph7 =[0.5324 0.8042 1.2571 1.6497; 0.5176 0.7847 1.2388 1.6128;0.5106 0.8173 1.2774 1.6499];
% 
% 

features = 4;

%% divide log ito 3 parts
% v1 = 43min
% each part =  14 min
%% ph3 first is the same for all
ph2_v1=[0.4785    0.7468    1.1940    1.5518;0.4702    0.7400    1.1895    1.5492; 0.4804 0.7398    1.1770    1.5452];
ph3_v1=[0.5577 0.8296 1.3486 1.7194; 0.5134 0.8190 1.3285 1.7021;0.5361 0.8226 1.3295 1.7041 ];
ph4_v1=[0.4992 0.7248 1.1761 1.4957; 0.5010 0.7389 1.1751 1.5122;0.5063 0.7361 1.1765 1.5020];
ph5_v1=[0.544304853704313,0.810838263852238,1.302898031688126,1.769059917006335;0.544304853704313, 0.808296936412571,1.321193975635915,1.782801310936924;0.544304853704313, 0.803594965922307,1.298125328751080,1.766627777746760];
ph6_v1=[0.4993 0.7843 1.2448 1.6175;0.5254    0.8021    1.2402    1.6091;0.5007    0.7909    1.2469    1.6200];
ph7_v1=[0.5342    0.8262    1.2754    1.6348;0.5138    0.8226    1.2549    1.6564; 0.5399    0.8013    1.2589    1.6511];
%%%%%

% without divide
% 48minutes
ph2_v2 =[0.4859 0.7305 1.1925 1.6001];
ph3_v2 =[ 0.5299 0.7998 1.3397 1.7252];
ph4_v2 =[ 0.4929 0.7196 1.1319 1.4617];
ph5_v2=[0.5227 0.7983 1.2995 1.7756];
ph6_v2=[0.4765 0.7406 1.2028 1.6650];
ph7_v2=[0.5176 0.7847 1.2388 1.6128];

%% divide log ito 3 parts
%% v3 = 56.9459 min
%% 19 min every part
ph2_v3 = [0.4711 0.7016    1.1956   1.5662;0.4795    0.6892    1.1986    1.5582; 0.4660    0.7059    1.1857    1.5588];
ph3_v3 = [0.5299 0.8385    1.3370   1.7168; 0.5473    0.8238    1.3075    1.7221;0.5526    0.8383    1.3382    1.7190];
ph4_v3 = [  0.5037    0.7127    1.1307    1.4650; 0.5004    0.7257    1.1388    1.4768;0.4949    0.7367    1.1460    1.4808];
ph5_v3 = [0.5318 0.8012    1.2825    1.7385;0.5318 0.8014    1.2892    1.7283;0.5318 0.7945    1.2857    1.7524];
ph6_v3 = [     0.4930    0.7423    1.2184    1.5812 ;0.4868    0.7335    1.2064    1.5764; 0.4886    0.7335    1.2029    1.5703];
ph7_v3=  [   0.5333    0.8021    1.2725    1.6533;0.5151    0.7913    1.2748    1.6431;0.5327    0.8173    1.2771    1.6492];

%% duration = 60m
ph2_v4 =[0.4696    0.7374    1.1838    1.6078]; 
ph3_v4 =[0.5397    0.8376    1.3340    1.7311];
ph4_v4 =[0.4941    0.7311    1.1457    1.4814];
ph5_v4 =[0.5431    0.8165    1.3136    1.7859];
ph6_v4 =[0.5019    0.7987    1.2325    1.5978];
ph7_v4 =[0.5207    0.7949    1.2517    1.6401];

%% 72.0119
ph2_v5 =[0.4751    0.7198    1.1869    1.5874]; 
ph3_v5 =[0.5361    0.8165    1.3212    1.7138];
ph4_v5 =[0.4972    0.7286    1.1529    1.5000];
ph5_v5 =[0.5272    0.8206    1.2852    1.7742];
ph6_v5 =[0.5136    0.7830    1.2321    1.5914];
ph7_v5 =[0.5078    0.7957    1.2386    1.6150];

%68 minutes
ph2_v6 =[0.4778    0.7431    1.1853    1.6054]; 
ph3_v6 =[0.5263    0.8305    1.3282    1.7429];
ph4_v6 =[0.4895    0.7290    1.1682    1.5075];
ph5_v6 =[0.5331    0.8006    1.3087    1.7633];
ph6_v6 =[0.5206    0.7804    1.2421    1.6173];
ph7_v6 =[0.5124    0.8033    1.2508    1.6312];

%58
ph2_v7 =[0.4674    0.7321    1.1733    1.5924]; 
ph3_v7 =[0.5217    0.8179    1.3117    1.7067 ];
ph4_v7 =[0.4985    0.7327    1.1620    1.4938];
ph5_v7 =[0.5392    0.8101    1.3027    1.7706];
ph6_v7 =[0.5045    0.7890    1.2269    1.5990];
ph7_v7 =[0.5140    0.7899    1.2498    1.6176];

%52
ph2_v8 =[0.4772    0.7225    1.1686    1.5701]; 
ph3_v8 =[0.5136    0.8122    1.3099    1.7080];
ph4_v8 =[0.4813    0.7310    1.1263    1.4593];
ph5_v8 =[0.5169    0.8109    1.2652    1.7463];
ph6_v8 =[0.5016    0.7669    1.2092    1.5852];
ph7_v8 =[0.5068    0.7861    1.2283    1.6007];
%%%%%%%%%%%%
ph2 = [  ph2_v1 ;ph2_v2;ph2_v3;ph2_v4 ;ph2_v5;ph2_v6;ph2_v7;ph2_v8];
ph3 = [  ph3_v1 ;ph3_v2;ph3_v3;ph3_v4 ;ph3_v5;ph3_v6;ph3_v7;ph3_v8];
ph4 = [  ph4_v1 ;ph4_v2;ph4_v3;ph4_v4 ;ph4_v5;ph4_v6;ph4_v7;ph4_v8];
ph5 = [  ph5_v1 ;ph5_v2;ph5_v3;ph5_v4 ;ph5_v5;ph5_v6;ph5_v7;ph5_v8];
ph6 = [  ph6_v1 ;ph6_v2;ph6_v3;ph6_v4 ;ph6_v5;ph6_v6;ph6_v7;ph6_v8];
ph7 = [  ph7_v1 ;ph7_v2;ph7_v3;ph7_v4 ;ph7_v5;ph7_v6;ph7_v7;ph7_v8];



[ var_feat,lev_4, lev_3,lev_2,lev_1]  = get_feat( ph2 );
if features ==1
    plot(lev_3,lev_4,'r+')
elseif features ==2
    plot(lev_3,var_feat,'r+')
elseif features ==3
    plot(lev_4,var_feat,'r+')
else
    scatter3(lev_3,lev_4,var_feat,'r+')
end
var_feat_ph2 = var_feat;
lev_4_ph2 = lev_4;
lev_3_ph2 = lev_3;
lev_2_ph2 = lev_2;
lev_1_ph2 = lev_1;

hold on

[ var_feat,lev_4, lev_3,lev_2,lev_1]  = get_feat( ph3 );
if features ==1
    plot(lev_3,lev_4,'b*')
elseif features ==2
    plot(lev_3,var_feat,'b*')
elseif features ==3
    plot(lev_4,var_feat,'b*')
else
    scatter3(lev_3,lev_4,var_feat,'b*')
end
var_feat_ph3 = var_feat;
lev_4_ph3 = lev_4;
lev_3_ph3 = lev_3;
lev_2_ph3 = lev_2;
lev_1_ph3 = lev_1;

%%%%%%%%
[ var_feat,lev_4, lev_3,lev_2,lev_1]  = get_feat( ph4 );
if features ==1
    plot(lev_3,lev_4,'Ko')
elseif features ==2
    plot(lev_3,var_feat,'Ko')
elseif features ==3
    plot(lev_4,var_feat,'Ko')
else
    scatter3(lev_3,lev_4,var_feat,'Ko')
end
var_feat_ph4 = var_feat;
lev_4_ph4 = lev_4;
lev_3_ph4 = lev_3;
lev_2_ph4 = lev_2;
lev_1_ph4 = lev_1;

%%%%%%
[ var_feat,lev_4, lev_3,lev_2,lev_1]  = get_feat( ph5 );
if features ==1
    plot(lev_3,lev_4,'cd')
elseif features ==2
    plot(lev_3,var_feat,'cd')
elseif features ==3
    plot(lev_4,var_feat,'cd')
else
    scatter3(lev_3,lev_4,var_feat,'cd')
end
var_feat_ph5 = var_feat;
lev_4_ph5 = lev_4;
lev_3_ph5 = lev_3;
lev_2_ph5 = lev_2;
lev_1_ph5 = lev_1;
%%%%%%
[ var_feat,lev_4, lev_3,lev_2,lev_1]  = get_feat( ph6 );
if features ==1
    plot(lev_3,lev_4,'g^')
elseif features ==2
    plot(lev_3,var_feat,'g^')
elseif features ==3
    plot(lev_4,var_feat,'g^')
else
    scatter3(lev_3,lev_4,var_feat,'g^')
end
var_feat_ph6 = var_feat;
lev_4_ph6 = lev_4;
lev_3_ph6 = lev_3;
lev_2_ph6 = lev_2;
lev_1_ph6 = lev_1;
%%%%%%
[ var_feat,lev_4, lev_3,lev_2,lev_1] = get_feat( ph7 );
if features ==1
    plot(lev_3,lev_4,'ms')
elseif features ==2
    plot(lev_3,var_feat,'ms')
elseif features ==3
    plot(lev_4,var_feat,'ms')
else
    scatter3(lev_3,lev_4,var_feat,'ms')
end
var_feat_ph7 = var_feat;
lev_4_ph7 = lev_4;
lev_3_ph7 = lev_3;
lev_2_ph7 = lev_2;
lev_1_ph7 = lev_1;


level_1 = [ mean(lev_1_ph2) ; mean(lev_1_ph3) ; mean(lev_1_ph4); mean(lev_1_ph5) ;mean(lev_1_ph6); mean(lev_1_ph7)];
level_2 = [ mean(lev_2_ph2) ; mean(lev_2_ph3) ; mean(lev_2_ph4); mean(lev_2_ph5); mean(lev_2_ph6); mean(lev_2_ph7)];
level_3 = [ mean(lev_3_ph2) ; mean(lev_3_ph3) ; mean(lev_3_ph4) ;mean(lev_3_ph5) ;mean(lev_3_ph6) ;mean(lev_3_ph7)];
level_4 = [ mean(lev_4_ph2) ; mean(lev_4_ph3) ; mean(lev_4_ph4); mean(lev_4_ph5); mean(lev_4_ph6); mean(lev_4_ph7)];


x = [ 1,2,3,4];
%%%%%%
hold off

if features ==1
    xlabel('Feature 1')
    ylabel('Feature 2')
elseif features ==2
    xlabel('Feature 1')
    ylabel('Feature 3')
elseif features ==3
    xlabel('Feature 2')
    ylabel('Feature 3')
else
    xlabel('Feature 1')
    ylabel('Feature 2')
    zlabel('Feature 3')
end

legend('Phone1','Phone2','Phone3','Phone4','Phone5','Phone6','Phone7')