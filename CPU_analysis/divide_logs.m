function [duration, finger_print]= divide_logs( phone_id,log_idx )
%DIVIDE_FP Summary of this function goes here
%   Detailed explanation goes here

data = importdata(sprintf('/Users/alanwar/Desktop/logs/five_phone_v2/log%d%04d.csv', phone_id,log_idx));

index_1 = floor(length(data)/3);
index_2 = floor(length(data)*2/3);

[ FP1 ] = find_FP( data(1:index_1,1:3) )
[ FP2 ] = find_FP( data(index_1+1:index_2,1:3) )
%[ FP3 ] = find_FP( data(index_2+1:index_3,1:3) )
[ FP4 ] = find_FP( data(index_2+1:end,1:3) ) 

finger_print=[ FP1 ; FP2 ; FP4];
ts = data(:, 1);
t = (ts-ts(1))/1e9;
duration = max(t)/(4*60);
end

