clear all
run CPU_mat_logs/full_fingerprint.m
CPU_X = [phone2_X; phone3_X; phone4_X; phone5_X; phone6_X;phone7_X];
CPU_y = [phone2_y, phone3_y, phone4_y, phone5_y, phone6_y, phone7_y];

% use leave-one-out cross validation of KNN classifier
accuracy  = classify_knn(CPU_X, CPU_y, 3)
