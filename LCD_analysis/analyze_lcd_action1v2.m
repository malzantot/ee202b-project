function [y, X] = analyze_lcd_action1v2
% Logs collected on June 13
phone1_action1_logs = [10043, 10044, 10045, 10046, 10047, 10048, 10049];
phone2_action1_logs = [20005, 20006, 20007, 20008, 20009, 20010, 20011];
phone3_action1_logs = [30008, 30009, 30010, 30011, 30012, 30013, 30014];
phone4_action1_logs = [40007, 40008, 40009, 40010, 40011, 40012, 40013];
phone6_action1_logs = [60007, 60008, 60009, 60010, 60011, 60012, 60013];


% Logs collected on June 15
%phone1_new_logs = [10001, 10002, 10003, 10004, 10005, 10006, 10007];
phone1_new_logs = [10008, 10009, 10010, 10011, 10012];%
phone2_new_logs  = [20012, 20013, 20014, 20015, 20016, 20017, 20018, 20019, 20020, 20021];
phone2_new_logs  = [20016, 20017, 20018, 20019, 20020, 20021]; % 
phone3_new_logs  = [30015, 30016, 30017, 30018, 30019, 30020, 30021, 30022, 30023, 30024];
phone4_new_logs  = [40014, 40015, 40016, 40017, 40018, 40019, 40020, 40021, 40022, 40023];
phone6_new_logs  = [60014, 60015, 60016, 60017, 60018, 60019, 60020, 60021, 60022, 60023];
phone5_new_logs = [50001, 50002, 50003, 50004, 50005, 50006, 50007, 50008, 50009, 50010];
phone7_new_logs = [70001, 70002, 70003, 70004, 70005, 70006, 70007, 70008, 70009, 70010];


y = zeros(0, 1);
X = zeros(0, 2);


for log_file = phone1_action1_logs

    features = get_action1_features(log_file);
    scatter3(features(1), features(2), 1, 'r*')
    hold on
    y = [y; 1];
    X = [X; features];
end

for log_file = phone2_action1_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2), 1.0603, 'b*')
    y = [y; 2];
    X= [X; features];
end

for log_file = phone3_action1_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),1.1661, 'k*')
    y = [y; 3];
    X= [X; features];
end


for log_file = phone4_action1_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),0.9933, 'c*')
    y = [y; 4];
    X= [X; features];
end


for log_file = phone6_action1_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2), 1.1098, 'g*')
    y = [y; 6];
    X= [X; features];
end

for log_file = phone1_new_logs
    features = get_action1_features(log_file);
    y = [y; 1];
    X= [X; features];
    scatter3(features(1), features(2), 1, 'ro')
end

for log_file = phone2_new_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),1.0603, 'bo')
    y = [y; 2];
    X= [X; features];
end

for log_file = phone3_new_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),1.1661, 'ko')
    y = [y; 3];
    X= [X; features];
end

for log_file = phone4_new_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),0.9933, 'co')
    y = [y; 4];
    X= [X; features];
end

for log_file = phone5_new_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),1.2494, 'mo')
    y = [y; 5];
    X= [X; features];
end

for log_file = phone6_new_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),1.1098, 'go')
    y = [y; 6];
    X= [X; features];
end


for log_file = phone7_new_logs
    features = get_action1_features(log_file);
    scatter3(features(1), features(2),1.1173, 'yo')
    y = [y; 7];
    X= [X; features];
end
grid on

end






