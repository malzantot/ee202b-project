function accuracy  = classify_knn(X, y, k)
if (nargin < 3)
    k = 5;
end

CVO = cvpartition(length(y),'LeaveOut');
num_true = 0;

for i = 1:CVO.NumTestSets
    trIdx = CVO.training(i);
    teIdx = CVO.test(i);
    trainingX = X(trIdx,:);
    testingX = X(teIdx, :);
    trainingY = y(trIdx);
    testingY = y(teIdx);
    clf =  ClassificationKNN.fit(trainingX, trainingY, 'NumNeighbors', k);
    pred_y = predict(clf, testingX);
    if (pred_y == testingY)
        num_true = num_true + 1;       
    end
end

CVO.NumTestSets
accuracy = num_true / CVO.NumTestSets;
    