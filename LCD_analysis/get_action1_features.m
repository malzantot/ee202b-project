function features = get_action1_features(log_file)
[log_t, log_p] = plot_power(log_file, 0);
max_t = max(log_t);
    
part1 = log_p (log_t > 5 & log_t < 15 ); % brightness level = 0
    
% brightness level from 0 -> 255
part2 = log_p (log_t > 15 & log_t < 25);
part3 = log_p (log_t > 25 & log_t < 35); % brightness level = 255
d1= (mean(part3) -mean(part1));
d2 = max(get_moving_variance(part2));  
features=[d1, d2];
end