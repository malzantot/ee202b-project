function [X, y] = analyze_lcd_action1
% Logs collected on June 13
phone1_action1_logs = [10043, 10044, 10045, 10046, 10047, 10048, 10049];
phone2_action1_logs = []%20005, 20006, 20007, 20008, 20009, 20010, 20011];
phone3_action1_logs = [30008, 30009, 30010, 30011, 30012, 30013, 30014];
phone4_action1_logs = [40007, 40008, 40009, 40010, 40011, 40012, 40013];
phone6_action1_logs = [60007, 60008, 60009, 60010, 60011, 60012, 60013];


% Logs collected on June 15
phone1_new_logs = [10008, 10009, 10010, 10011, 10012];%
phone2_new_logs  = [20016, 20017, 20018, 20019, 20020, 20021]; % 
phone3_new_logs  = [30015, 30016, 30017, 30018, 30019, 30020, 30021, 30022, 30023, 30024];
phone4_new_logs  = [40014, 40015, 40016, 40017, 40018, 40019, 40020, 40021, 40022, 40023];
phone6_new_logs  = [];%60014, 60015, 60016, 60017, 60018, 60019, 60020, 60021, 60022, 60023];
phone5_new_logs = [50001, 50002, 50003, 50004, 50005, 50006, 50007, 50008, 50009, 50010];
phone7_new_logs = [70001, 70002, 70003, 70004, 70005, 70006, 70007, 70008, 70009, 70010];


% Logs collected on June 16
phone2_last_logs = 20022:20031;
phone3_last_logs = 30029:30033;
phone4_last_logs = 40024:40033;
phone5_last_logs = 50011:50020;
phone6_last_logs = 60024:60033;
phone7_last_logs = 70011:70020;



y = zeros(0, 1);
X = zeros(0, 2);

figure
hold on
for log_file = phone1_action1_logs

    features = get_action1_features(log_file);
    plot(features(1), features(2), 'r*', 'MarkerSize', 8)
    y = [y; 1];
    X= [X; features];
end

for log_file = phone2_action1_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'b*', 'MarkerSize', 8)
    y = [y; 2];
    X= [X; features];
end

for log_file = phone3_action1_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'k*', 'MarkerSize', 8)
    y = [y; 3];
    X= [X; features];
end


for log_file = phone4_action1_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'c*', 'MarkerSize', 8)
    y = [y; 4];
    X= [X; features];
end


for log_file = phone6_action1_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'g*', 'MarkerSize', 8)
    
    y = [y; 6];
    X= [X; features];
end


% new
for log_file = phone1_new_logs
    features = get_action1_features(log_file);
    y = [y; 1];
    X= [X; features];
    plot(features(1), features(2), 'ro', 'MarkerSize', 8)
end

for log_file = phone2_new_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'bo', 'MarkerSize', 8)
    y = [y; 2];
    X= [X; features];
end

for log_file = phone3_new_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'ko', 'MarkerSize', 8)
    y = [y; 3];
    X= [X; features];
end

for log_file = phone4_new_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'co', 'MarkerSize', 8)
    y = [y; 4];
    X= [X; features];
end

for log_file = phone5_new_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'mo', 'MarkerSize', 8)
    y = [y; 5];
    X= [X; features];
end

for log_file = phone6_new_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'go', 'MarkerSize', 8)
    y = [y; 6];
    X= [X; features];
end


for log_file = phone7_new_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'yo', 'MarkerSize', 8)
    y = [y; 7];
    X= [X; features];
end

% last
for log_file = phone2_last_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'bx', 'MarkerSize', 8)
    y = [y; 2];
    X= [X; features];
end

for log_file = phone3_last_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'kx', 'MarkerSize', 8)
    y = [y; 3];
    X= [X; features];
end

for log_file = phone4_last_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'cx', 'MarkerSize', 8)
    y = [y; 4];
    X= [X; features];
end

for log_file = phone5_last_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'mx', 'MarkerSize', 8)
    y = [y; 5];
    X= [X; features];
end

for log_file = phone6_last_logs
    features = get_action1_features(log_file);
            if (features(1) > 0.65)

    plot(features(1), features(2), 'gx', 'MarkerSize', 8)

    y = [y; 6];
    X= [X; features];
        end
end


for log_file = phone7_last_logs
    features = get_action1_features(log_file);
    plot(features(1), features(2), 'y-', 'MarkerSize', 8)
    y = [y; 7];
    X= [X; features];
end

axis([0.7 0.93 0.05 0.25])
xlabel('Power Jump')
ylabel('Max. Variance')
grid on

inter_device_var  = var(X)
intra_device_var = var(X(y==1, :));
for i = 2:7
    intra_device_var = max(intra_device_var, var(X(y==i, :)));
end
intra_device_var

end






