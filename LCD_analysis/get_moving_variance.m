function moving_var  = get_moving_variance(input_x, win_size)
if (nargin < 2)
    win_size = 16;
end
moving_var = [];
log_len = length(input_x);
for k = 1:log_len
        start_idx = max(1, k-win_size);
        end_idx = min(log_len, k+win_size);
        moving_var(k) = var(input_x(start_idx:end_idx));
end
end