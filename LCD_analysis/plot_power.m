% Plot the measured current
function [t, inst_power] = plot_power(log_idx, plot_on)
if (nargin <2)
    plot_on = 1;
end

data = importdata(sprintf('log%04d.csv', log_idx));
ts = data(:, 1);
t = (ts-ts(1))/1e9;
inst_power = (data(:,2)/1e6) .* (data(:,3)/1e6);

%figure
%plot(t, data(:, 2) / 1e6, 'r') % API current_now
%hold on
%plot(t, data(:, 4) / 1e6, 'b') % Logical file current_now
%grid on
if (plot_on==1)
figure
subplot(2, 1, 1)
title('Power')
plot(t, smooth(inst_power, 16), 'b');
grid on
end
sample_var = get_moving_variance(inst_power, 16);

if (plot_on == 1)
    subplot(2, 1, 2)
    plot(t, sample_var)
    grid on
end
end
