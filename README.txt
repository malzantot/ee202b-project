#############################
#  PowerPrint: Fingerprinting Mobile Phones using Power Consumption
Characteristics      
#  EE202B Project, Spring 2015
#
###########################

The contents of this folder are:

PowerPrint: Android data collection application.
CPU_Analysis: CPU experiments logs and matlab scripts
LCD_Analysis: LCD experiments logs and matlab scripts
Report: PDF file and LaTex source of our report
Slides: Our Project slides.


