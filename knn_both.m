phone2_lcd = datasample(X(y==2,:), size(phone2_X, 1));
phone2_X_both = [phone2_X , phone2_lcd];

phone3_lcd = datasample(X(y==3,:), size(phone3_X, 1));
phone3_X_both = [phone3_X , phone3_lcd];

phone4_lcd = datasample(X(y==4,:), size(phone4_X, 1));
phone4_X_both = [phone4_X , phone4_lcd];

phone5_lcd = datasample(X(y==5,:), size(phone5_X, 1));
phone5_X_both = [phone5_X , phone5_lcd];

phone6_lcd = datasample(X(y==6,:), size(phone6_X, 1));
phone6_X_both = [phone6_X , phone6_lcd];

phone7_lcd = datasample(X(y==7,:), size(phone7_X, 1));
phone7_X_both = [phone7_X , phone7_lcd];

both_X =[phone2_X_both; phone3_X_both; phone4_X_both; phone5_X_both; phone6_X_both;phone7_X_both];
both_y = [phone2_y, phone3_y, phone4_y, phone5_y, phone6_y, phone7_y];

accuracy = classify_knn(both_X, both_y, 3)