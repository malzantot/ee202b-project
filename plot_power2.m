function plot_power2(log1_idx, log2_idx)
data1 = importdata(sprintf('log%04d.csv', log1_idx));
data2 = importdata(sprintf('log%04d.csv', log2_idx));
ts1 = data1(:, 1);
t1 = (ts1-ts1(1))/1e9;
inst_power1 = (data1(:,2)/1e6) .* (data1(:,3)/1e6);

ts2 = data2(:, 1);
t2 = (ts2-ts2(1))/1e9;
inst_power2 = (data2(:,2)/1e6) .* (data2(:,3)/1e6);

%figure
%plot(t, data(:, 2) / 1e6, 'r') % API current_now
%hold on
%plot(t, data(:, 4) / 1e6, 'b') % Logical file current_now
%grid on
figure
subplot(2, 1, 1)
title('Instanteous Power')
plot(t1, smooth(inst_power1, 50), 'b');
hold on
plot(t2, smooth(inst_power2, 50), 'r');
xlabel('Time (Sec)')
ylabel('Power')
grid on


sample_var1 = [];
sample_var2 = [];
win_size = 50
log_len1 = length(inst_power1)
for k = 1:log_len1
        start_idx = max(1, k-win_size);
        end_idx = min(log_len1, k+win_size);
        sample_var1(k) = var(inst_power1(start_idx:end_idx));
end
log_len2 = length(inst_power2)
for k = 1:log_len2
        start_idx = max(1, k-win_size);
        end_idx = min(log_len2, k+win_size);
        sample_var2(k) = var(inst_power2(start_idx:end_idx));
end
    subplot(2, 1, 2)
    hold on
    plot(t1, sample_var1, 'b')
    plot(t2, sample_var2, 'r')
    xlabel('Time (Sec.)')
    ylabel('Variance of Power')
    grid on
end
